#pragma once

#include <QObject>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>
#include "types.h"
#include <QVector2D>

struct DVVertex {
    int16_t x;
    int16_t y;
    int16_t z;
    float u;
    float v;
};

struct DVFaceLinedef {
    bool block_sound;
    bool block_monsters;
    bool upper_unpeg;
    bool two_sided;
    bool automap;
    bool lower_unpeg;
    bool invisible;
    bool impassable;
    bool secret;
    int16_t back;
    int16_t front;
    int tag;
    int flags;
    int vx_b;
    int vx_a;
    int action;
};

struct DVFace {
    QVector<DVVertex> vertices;
    QString texture;

};


class DVMapFile {
public:
    QFile *m_file = nullptr;
    QVector<DVFace> m_faces;

    DVMapFile(QString path) {
        m_file = new QFile(path);
        if (!m_file->open(QFile::ReadOnly)) {
            qDebug() << "Failed to open file" << path;
            return;
        }

        QJsonDocument json(QJsonDocument::fromJson(m_file->readAll().data()));

        QJsonObject map = json.object();

        if (!map.contains("map")) {
            throw "Isn't a DoomView map!";
        }

        QJsonArray faces = map["faces"].toArray();

        for (auto faceref : faces) {
            QJsonObject face = faceref.toObject();

            DVFace f;
            f.texture = face["texture"].toString();

            for (auto vertexref : face["vertices"].toArray()) {
                QJsonArray vertex = vertexref.toArray();

                DVVertex v;

                v.x = vertex[0].toInt();
                v.y = vertex[1].toInt();
                v.z = vertex[2].toInt();
                v.u = vertex[3].toDouble();
                v.v = 1.0 - vertex[4].toDouble();

                f.vertices.append(v);
            }

            m_faces.append(f);
        }
    }

    ~DVMapFile() {
        m_file->close();
        delete m_file;
    }
};
