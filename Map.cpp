#pragma once

#include <QObject>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QResource>

#include "types.h"

class Map {
public:
    QVector<DoomVertex> vertices;
    QVector<DoomLinedef> linedefs;
    QVector<DoomSidedef> sidedefs;
    QVector<DoomSector> sectors;
    QVector<DoomSubSectors> ssectors;
    QVector<DoomSeg> segs;

    void load() {
        loadVertices();
        loadLines();
        loadSides();
        loadSectors();
        loadSubSectors();
        loadSegs();
    }

    void loadVertices() {
        QFile file("/home/prophessor/Projects/DoomView/map/VERTEXES.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomVertex);
        vertices.resize(size);

        file.read((char *)vertices.data(), sizeof(DoomVertex) * size);

        file.close();
    }

    void loadLines() {
        QFile file("/home/prophessor/Projects/DoomView/map/LINEDEFS.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomLinedef);
        linedefs.resize(size);

        file.read((char *)linedefs.data(), sizeof(DoomLinedef) * size);

        file.close();
    }

    void loadSides() {
        QFile file("/home/prophessor/Projects/DoomView/map/SIDEDEFS.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomSidedef);
        sidedefs.resize(size);

        file.read((char *)sidedefs.data(), sizeof(DoomSidedef) * size);

        file.close();
    }

    void loadSectors() {
        QFile file("/home/prophessor/Projects/DoomView/map/SECTORS.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomSector);
        sectors.resize(size);

        file.read((char *)sectors.data(), sizeof(DoomSector) * size);

        file.close();
    }

    void loadSubSectors() {
        QFile file("/home/prophessor/Projects/DoomView/map/SSECTORS.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomSubSectors);
        ssectors.resize(size);

        file.read((char *)ssectors.data(), sizeof(DoomSubSectors) * size);

        file.close();
    }

    void loadSegs() {
        QFile file("/home/prophessor/Projects/DoomView/map/SEGS.lmp");
        file.open(QFile::ReadOnly);

        uint16_t size = file.size() / sizeof(DoomSeg);
        segs.resize(size);

        file.read((char *)segs.data(), sizeof(DoomSeg) * size);

        file.close();
    }
};
