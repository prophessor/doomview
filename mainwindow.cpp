#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) {
    setSurfaceType(QWindow::OpenGLSurface);

    setMouseGrabEnabled(true);
    QSurfaceFormat format;
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    format.setVersion(2, 1);
    setFormat(format);

    m_map = new Map();

    m_map->load();

    openFileDialog();
}

MainWindow::~MainWindow() {
    delete m_map;
    delete m_dvmap;
}

void MainWindow::openFileDialog() {
    QString dir = QDir::currentPath();

    QString path = QFileDialog::getOpenFileName(nullptr, "Select DoomView map", dir, "*.json");

    if(!path.isEmpty()) {
        delete m_dvmap;
        m_dvmap = new DVMapFile(path);

        QFileInfo info(path);
        setTitle(info.fileName() + " - DoomView");
    }
}

void MainWindow::initializeGL() {
    glClearColor(1.f, 1.f, 1.f, 1.f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    renderer.initShaders(this);

    resizeGL(width(), height());
}

void MainWindow::resizeGL(int width, int height) {
    glViewport(0, 0, width, height);

    float aspectRatio = width / float(height);

    m_camera.aspectRatio = aspectRatio;
}

void MainWindow::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 modelMatrix;

    modelMatrix.setToIdentity();
    modelMatrix.scale(.001f, .001f, .001f);

    renderer.m_shaderProgram.bind();
    renderer.m_shaderProgram.setUniformValue("modelMatrix", modelMatrix);
    renderer.m_shaderProgram.setUniformValue("viewMatrix", m_camera.viewMatrix());
    renderer.m_shaderProgram.setUniformValue("projectionMatrix", m_camera.projectionMatrix());

    /*

    glBegin(GL_LINES);


    for (auto &segment : m_map->segs) {
        glVertex2d(m_map->vertices[segment.v1].x, m_map->vertices[segment.v1].y);
        glVertex2d(m_map->vertices[segment.v2].x, m_map->vertices[segment.v2].y);
    }

    glEnd();

    */


    for (auto &face : m_dvmap->m_faces) {
        QVector<Vertex> tmpVertices;
        renderer.setTextureLocal(face.texture + ".png");

        for (auto &vertex : face.vertices) {
            tmpVertices.append(Vertex(vertex));
        }

        renderer.renderFace(tmpVertices);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if(event->key() == Qt::Key_A) {
        m_camera.move(step, QVector3D(1, 0, 0));
    }
    if(event->key() == Qt::Key_D) {
        m_camera.move(step, QVector3D(-1, 0, 0));
    }
    if(event->key() == Qt::Key_W) {
        m_camera.move(step, QVector3D(0, 0, 1));
    }
    if(event->key() == Qt::Key_S) {
        m_camera.move(step, QVector3D(0, 0, -1));
    }
    if(event->key() == Qt::Key_Q) {
        m_camera.move(step, QVector3D(0, -1, 0));
    }
    if(event->key() == Qt::Key_E) {
        m_camera.move(step, QVector3D(0, 1, 0));
    }

    if(event->matches(QKeySequence::MoveToNextChar)) {
        m_camera.rotate(rstep, QVector3D(0, 1, 0));
    }
    if(event->matches(QKeySequence::MoveToPreviousChar)) {
        m_camera.rotate(-rstep, QVector3D(0, 1, 0));
    }

    update();
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    return;
    //qDebug() << "mouseMove" << event->x() << event->y();
    if (prevMx == 65535) {
        prevMx = event->x();
        prevMy = event->y();
        return;
    }

    int16_t dx = event->x() - prevMx;
    int16_t dy = event->y() - prevMy;

    prevMx = event->x();
    prevMy = event->y();

    m_camera.rotate(dx * sensivity, QVector3D(0, 1, 0));
    m_camera.rotate(dy * sensivity, QVector3D(1, 0, 0));

    update();
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    resizeGL(width(), height());
    update();
}

void MainWindow::paintEvent(QPaintEvent *event) {
    paintGL();
}

