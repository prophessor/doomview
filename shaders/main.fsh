uniform sampler2D qt_Texture0;

varying highp vec2 _texCoord;
varying highp vec4 _color;

void main(void) {
    gl_FragColor = texture2D(qt_Texture0, _texCoord.st);
}
