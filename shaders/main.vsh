uniform highp mat4 modelMatrix;
uniform highp mat4 viewMatrix;
uniform highp mat4 projectionMatrix;

attribute highp vec2 texCoord;
attribute highp vec4 color;

varying highp vec2 _texCoord;
varying highp vec4 _color;

void main(void) {
    mat4 modelViewProjectionMatrix = projectionMatrix * viewMatrix * modelMatrix;
    _color = color;
    _texCoord = texCoord;
    gl_Position = modelViewProjectionMatrix * gl_Vertex;
}
