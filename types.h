#pragma once

#include <QObject>
#include <QFile>
#include "DoomViewMapFile.cpp"

struct Vertex {
    Vertex(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {};
    Vertex(float _x, float _y, float _z, float _u, float _v) : x(_x), y(_y), z(_z), u(_u), v(_v) {};
    Vertex(DVVertex v) : x(v.x), y(v.y), z(v.z), u(v.u), v(v.v) {}


    float x;
    float y;
    float z;
    float u;
    float v;
};

struct Face {
    Face() : v1(0), v2(0), v3(0) {};
    Face(int16_t _v1, int16_t _v2, int16_t _v3) : v1(_v1), v2(_v2), v3(_v3) {};

    uint16_t v1;
    uint16_t v2;
    uint16_t v3;
};

struct DoomVertex {
    DoomVertex() : x(0), y(0) {};
    DoomVertex(int16_t _x, int16_t _y) : x(_x), y(_y) {};
    DoomVertex(QFile *file) {
        parse(file);
    };

    void parse(QFile *file) {
        file->read((char *)&x, sizeof(int16_t));
        file->read((char *)&y, sizeof(int16_t));
    }

    int16_t x;
    int16_t y;
} Q_PACKED;

struct DoomLinedef {
    uint16_t v1;
    uint16_t v2;
    uint16_t flags;
    uint16_t type;
    uint16_t sector_tag;
    uint16_t side1;
    uint16_t side2;
} Q_PACKED;

struct DoomSidedef {
  int16_t   textureoffset;
  int16_t   rowoffset;
  char      toptexture[8];
  char      bottomtexture[8];
  char      midtexture[8];
  uint16_t  sector;  // Front sector, towards viewer.
} Q_PACKED;

struct DoomSector {
  int16_t floorheight;
  int16_t ceilingheight;
  char  floorpic[8];
  char  ceilingpic[8];
  int16_t lightlevel;
  uint16_t special;
  uint16_t tag;
} Q_PACKED;

struct DoomSubSectors {
    int16_t amount;
    int16_t first;
} Q_PACKED;

struct DoomSeg {
    uint16_t v1;
    uint16_t v2;
    int16_t angle;
    uint16_t linedef;
    int16_t direction;
    int16_t distance;
} Q_PACKED;
