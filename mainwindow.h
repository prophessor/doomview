#pragma once
#include <QMainWindow>
#include <QMatrix4x4>
#include <QSurfaceFormat>
#include <QFileDialog>
#include <QDir>

#include <QOpenGLWindow>
#include <QOpenGLFunctions>
#include <QtOpenGL/QGLContext>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QKeyEvent>
#include "Map.cpp"
#include "DoomViewMapFile.cpp"
#include "Camera.cpp"
#include "Renderer.cpp"

class MainWindow : public QOpenGLWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void openFileDialog();

protected:
    virtual void initializeGL();
    virtual void resizeGL(int width, int height);
    virtual void paintGL();

    void keyPressEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    QOpenGLShaderProgram m_shaderProgram;
    Camera        m_camera;

    Map *m_map = nullptr;
    DVMapFile *m_dvmap = nullptr;
    Renderer renderer;

    uint16_t prevMx = 65535;
    uint16_t prevMy = 65535;

    float step = 0.1;
    float rstep = 2;
    float sensivity = 1;
};
