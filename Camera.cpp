#pragma once

#include <QObject>
#include <QVector3D>
#include <QMatrix4x4>

class Camera {
public:
    QVector3D position;

    QMatrix4x4 m_view;
    QMatrix4x4 m_rot;
    QMatrix4x4 m_proj;

    float aspectRatio = 1.3333;

    Camera() {
        m_rot.setToIdentity();
    }

    void move(float offset, QVector3D direction) {
        position += direction * offset * m_rot;
    }

    void rotate(float angle, QVector3D vector) {
        m_rot.rotate(angle, vector);
    }

    QMatrix4x4 viewMatrix() {
        m_view.setToIdentity();
        m_view.translate(position);
        return m_rot * m_view;
    };

    QMatrix4x4 projectionMatrix() {
        m_proj.setToIdentity();
        m_proj.perspective(45, aspectRatio, 0.00001f, 100000.f);
        return m_proj;
    };
};
