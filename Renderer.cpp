#pragma once

#include <QObject>
#include <QWindow>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include "types.h"

enum attribute {
    color = 1,
    texCoord = 2,
};

class Renderer {
public:
    QOpenGLShaderProgram m_shaderProgram;
    QHash<QString, QOpenGLTexture *> m_textures;

    void initShaders(QWindow *parent) {
        bool vertexDone = m_shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/main.vsh");

        if (!vertexDone) {
            qDebug() << "Failed to load main.vsh";
            parent->close();
        }

        bool fragmentDone = m_shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/main.fsh");

        if (!fragmentDone) {
            qDebug() << "Failed to load main.fsh";
            parent->close();
        }

        m_shaderProgram.bindAttributeLocation("color", attribute::color);
        m_shaderProgram.bindAttributeLocation("texCoord", attribute::texCoord);

        if (!m_shaderProgram.link()) {
            qDebug() << "Failed to link shaders!";
            parent->close();
        }
    }

    void renderFace(QVector<Vertex> vertices) {
        glBegin(GL_TRIANGLE_FAN);

        for (Vertex &vertex : vertices) {
            m_shaderProgram.setAttributeValue(attribute::texCoord, QVector2D(vertex.u, vertex.v));
            glVertex3f(vertex.x, vertex.y, vertex.z);
        }

        glEnd();
    }

    void setTexture(QString path) {
        if (!m_textures.contains(path)) {
            m_textures.insert(path, new QOpenGLTexture(QImage(path)));
        }

        m_textures[path]->bind(0);
    }

    void setTextureLocal(QString name) {
        setTexture("/home/prophessor/tmp/omgifol/demo/" + name);
    }
};
